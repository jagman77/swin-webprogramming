﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace api1.Controllers
{
    public class ValuesController : ApiController
    {
        // GET api/values
        public string Get()
        {
            return "Hello World";
        }

        // GET api/values/5
        public string Get(int num1)
        {
            int total = 0;

            while (num1 >= 0)  {

                total = total + num1;

                num1 -= 1;
            }

            return (total).ToString();
        }

        // GET api/values/5
        public string Get(int num1, int num2)
        {
            return (num1 + num2).ToString();
        }

        // POST api/values
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        public void Delete(int id)
        {
        }
    }
}
